function SRSet = warpLRASet(SRSet,varargin)
%WARPLRASET Summary of this function goes here
%   Detailed explanation goes here

[Val,Lab,Pos,DispX,DispY] = SR.SetToFields(SRSet);

Mask = Lab;

for it = 1:numel(Lab)
    Mask{it} = Lab{it}==it;
end

for it = 1:numel(Val)
    [Val{it},Mask{it},Pos{it},DispX{it},DispY{it}] = SR.warpLRA(...
        Val{it},Mask{it},Pos{it},DispX{it},DispY{it},varargin{:});
end

for it = 1:numel(Lab)
    Lab{it} = it*double(Mask{it});
end

SRSet = SR.FieldsToSet(Val,Lab,Pos,DispX,DispY);
end