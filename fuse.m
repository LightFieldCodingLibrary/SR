function Set = fuse(VisSet,OccSet)
%FUSE Summary of this function goes here
%   Detailed explanation goes here

[OffVis,ColVis,LabVis] = SR.SetToFields(VisSet);
[OffOcc,ColOcc,LabOcc] = SR.SetToFields(OccSet);

if isempty(OccSet)
    Set = VisSet;
    return
elseif isempty(VisSet)
    Set = OccSet;
    return
end

numLab = max(numel(LabVis),numel(LabVis));
OffVis(end+1:numLab) = deal({[]});
OffOcc(end+1:numLab) = deal({[]});
ColVis(end+1:numLab) = deal({[]});
ColOcc(end+1:numLab) = deal({[]});
LabVis(end+1:numLab) = deal({[]});
LabOcc(end+1:numLab) = deal({[]});

for lab = 1:numLab
    LabVis{lab}(LabVis{lab}==0) = nan;
    LabOcc{lab}(LabOcc{lab}==0) = nan;
    
    [Cols,Offs,Labs] = utils.align({ColVis{lab},ColOcc{lab}},...
        {OffVis{lab},OffOcc{lab}},{LabVis{lab},LabOcc{lab}});
    
    for it = 1:numel(Labs)
        Labs{it} = it.*double(Labs{it}==lab);
    end
    
    [LabVis{lab},OffVis{lab},ColVis{lab}] = utils.join(Labs,Offs,Cols);
    
    LabVis{lab} = lab.*double(LabVis{lab}~=0);
end

Set = SR.FieldsToSet(OffVis,ColVis,LabVis);

end