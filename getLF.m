function [LFRef,LFDisp] = getLF(SRRef,SRX,SRY,lfxgv,lfygv,Method,numLay,SRDisp)
%GETLF Summary of this function goes here
%   Detailed explanation goes here

%% Initialize constants and interpolants
SRCol = {SRRef.Color};
SRLab = {SRRef.Label};
SRSize = size(SRRef(1).Color);
SRSize(end+1:5) = 1;

seh = strel('arbitrary',[1,0,1]);
sev = strel('arbitrary',[1,0,1]');

if ~exist('numLay','var'); numLay = 1; end
if ~exist('SRDisp','var'); SRDisp = cellfun(@(x) 0.*x,SRX,'UniformOutput',false); end

numChan = size(SRCol{1},3);
ImgSize = [numel(lfxgv),numel(lfygv)];
ImgRes = SRSize(4:end);
Offset = [min(lfxgv),min(lfygv),1,1,1]-1;
numLab = numel(SRCol);
numView = prod(ImgRes);

%% Reshape variables
Color  =  nan  ([ImgSize,numChan,numView,numLay]);
Label  =  zeros([ImgSize,1      ,numView,numLay]);
LFDisp = -inf  ([ImgSize,1      ,numView,numLay]);

%% Remove warnings
wgriddata = warning('query','MATLAB:griddata:DuplicateDataPoints');
wscattered = warning('query','MATLAB:scatteredInterpolant:DupPtsAvValuesWarnId');
warning('off','MATLAB:griddata:DuplicateDataPoints');
warning('off','MATLAB:scatteredInterpolant:DupPtsAvValuesWarnId');

%% Compute light field using interpolation
% Compute light field super-ray by super-ray
progress('',0);
for lab = 1:numLab
    % Replace missing values by zero (to avoid interpolation errors)
    for it = 1:2
        SRCol{lab} = utils.fillmissing(SRCol{lab},seh,1);
        SRDisp{lab} = utils.fillmissing(SRDisp{lab},seh,1);
        
        SRCol{lab} = utils.fillmissing(SRCol{lab},sev,1);
        SRDisp{lab} = utils.fillmissing(SRDisp{lab},sev,1);
    end
    
    SRCol {lab}(isnan(SRCol {lab})) = 0;
    SRLab {lab}(isnan(SRLab {lab})) = 0;
    SRDisp{lab}(isnan(SRDisp{lab})) = 0;
    
    % Compute super-ray view by view
    for v = 1:numView
        msg = ['Computing light field from super-ray ',num2str(lab),'/',num2str(numLab),...
            ' (view ',num2str(v),'/',num2str(numView),')\n'];
        progress(msg);
        
        SRXsub    = SRX   {lab}(:,:,1,v);
        SRYsub    = SRY   {lab}(:,:,1,v);
        SRColsub  = SRCol {lab}(:,:,:,v);
        SRLabsub  = SRLab {lab}(:,:,1,v);
        SRDispsub = SRDisp{lab}(:,:,1,v);
        
        SRColsub  = utils.fillmissing(SRColsub,seh,1);
        SRLabsub  = utils.fillmissing(SRLabsub,seh,1);
        SRDispsub = utils.fillmissing(SRDispsub,seh,1);
        
        SRColsub  = utils.fillmissing(SRColsub,sev,1);
        SRLabsub  = utils.fillmissing(SRLabsub,sev,1);
        SRDispsub = utils.fillmissing(SRDispsub,sev,1);
        
        % Compute super-ray boundary
        srxqmin = ceil (min(SRXsub(:)));
        srxqmax = floor(max(SRXsub(:)));
        sryqmin = ceil (min(SRYsub(:)));
        sryqmax = floor(max(SRYsub(:)));
        
        % Reduce number of query points for speed
        xgv = lfxgv>=srxqmin & lfxgv<=srxqmax;
        ygv = lfygv>=sryqmin & lfygv<=sryqmax;
        
        [LFXsub,LFYsub] = ndgrid(lfxgv(xgv),lfygv(ygv));
        
        [SRXsub,SRYsub,LFXsub,LFYsub] = utils.gridCoords(SRXsub,SRYsub,LFXsub,LFYsub);
        
        queryNaN = isnan(LFXsub)|isnan(LFYsub);
        
        LFXsub(queryNaN) = 1;
        LFYsub(queryNaN) = 1;
        
        % Interpolate disparity
        tempDisp = interp2(SRYsub,SRXsub,SRDispsub,LFYsub,LFXsub,Method);
        
        % Interpolate label
        tempLab = interp2(SRYsub,SRXsub,SRLabsub,LFYsub,LFXsub,'nearest');
        
        % Find out of boundary values in label using cubic interpolation
        LabNaN = isnan(interp2(SRYsub,SRXsub,SRLabsub,LFYsub,LFXsub,'cubic'));
        
        % Stencil buffer
        Mask = ~(queryNaN|LabNaN) & tempLab==lab;
        
        Mask = reshape(Mask,[],1);
        
        buffDisp = LFDisp(xgv,ygv,1,v,:);
        buffLab  = Label (xgv,ygv,1,v,:);
        
        szBuff = size(buffDisp);
        
        buffDisp = reshape(buffDisp,[],numLay);
        buffLab  = reshape(buffLab ,[],numLay);
        
        tempDisp = reshape(tempDisp,[],1);
        tempLab  = reshape(tempLab ,[],1);
        
        jointDisp = [buffDisp,tempDisp];
        jointLab  = [buffLab ,tempLab ];
        
        [~,sig] = sort(jointDisp,2,'descend');
        
        sigsz = size(sig);
        x = ndgrid(1:sigsz(1),1:sigsz(2));
        sig = sub2ind(sigsz,x,sig);
        
        jointDisp = jointDisp(sig);
        jointLab  = jointLab (sig);
        
        buffDisp(Mask,:) = jointDisp(Mask,1:numLay);
        buffLab (Mask,:) = jointLab (Mask,1:numLay);
        
        buffDisp = reshape(buffDisp,szBuff);
        buffLab  = reshape(buffLab ,szBuff);
        
        LFDisp(xgv,ygv,1,v,:) = buffDisp;
        Label (xgv,ygv,1,v,:) = buffLab;
        
        % Interpolate color
        for c = 1:numChan
            tempCol = interp2(SRYsub,SRXsub,SRColsub(:,:,c),LFYsub,LFXsub,Method);
            
            buffCol = Color(xgv,ygv,c,v,:);
            
            buffCol = reshape(buffCol,[],numLay);
            tempCol = reshape(tempCol,[],1);
            
            jointCol = [buffCol,tempCol];
            jointCol = jointCol(sig);
            
            buffCol(Mask,:) = jointCol(Mask,1:numLay);
            
            buffCol = reshape(buffCol,szBuff);
            
            Color(xgv,ygv,c,v,:) = buffCol;
        end
    end
end

progress('',0);
fprintf('\n');

%% Reset warnings
warning(wgriddata.state,'MATLAB:griddata:DuplicateDataPoints');
warning(wscattered.state,'MATLAB:scatteredInterpolant:DupPtsAvValuesWarnId');

%% Reshape variables
Color  = reshape(Color ,[ImgSize,numChan,ImgRes,numLay]);
Label  = reshape(Label ,[ImgSize,1      ,ImgRes,numLay]);
LFDisp = reshape(LFDisp,[ImgSize,1      ,ImgRes,numLay]);

%%
LFRef = SR.FieldsToSet(Offset,Color,Label);

%%
    function progress(msg,varargin)
        persistent sz
        if isempty(sz); sz = 0; end
        if nargin>1; sz = varargin{1}; end
        
        fprintf(repmat('\b',1,sz));
        fprintf(msg);
        sz = numel(msg)-count(msg,'\n');
    end
end