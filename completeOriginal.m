function Ref = completeOriginal(Ref)
%COMPLETEORIGINAL Summary of this function goes here
%   Detailed explanation goes here

[Offset,Color,Label] = SR.SetToFields(Ref);

numLab = numel(Label);

for lab = 1:numLab
    LFSize = size(Color{lab});
    ImgSize = LFSize(1:2);
    ImgRes = LFSize(4:end);
    
    ImgSizeC = num2cell(ImgSize);
    ImgResC  = num2cell(ImgRes);
    
    Label{lab} = reshape(Label{lab},prod(ImgSize),[]);
    Color{lab} = reshape(Color{lab},prod(ImgSize),[]);
    
    M = any(Label{lab}==lab,2);
    Label{lab}( M,:) = lab;
    Label{lab}(~M,:) = 0;
    Color{lab}(~M,:) = nan;
    
    Label{lab} = reshape(Label{lab},ImgSizeC{:},[],ImgResC{:});
    Color{lab} = reshape(Color{lab},ImgSizeC{:},[],ImgResC{:});
end

Ref = SR.FieldsToSet(Offset,Color,Label);

end