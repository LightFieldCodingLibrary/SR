function [VisRef,OccRef] = separate(Ref,Mask)
%SEPARATE Summary of this function goes here
%   Detailed explanation goes here

[Offset,ColVis,LabVis] = SR.SetToFields(Ref);
numLab = numel(Ref);

ColOcc = ColVis;
LabOcc = LabVis;

for lab = 1:numLab
    MVis = LabVis{lab}==lab &  Mask{lab};
    MOcc = LabOcc{lab}==lab & ~Mask{lab};
    
    LabVis{lab}( MVis) = lab;
    LabVis{lab}(~MVis) = 0;
    LabOcc{lab}( MOcc) = lab;
    LabOcc{lab}(~MOcc) = 0;
    
    MVis = MVis & true(size(ColVis{lab}));
    MOcc = MOcc & true(size(ColOcc{lab}));
    
    ColVis{lab}(~MVis) = nan;
    ColOcc{lab}(~MOcc) = nan;
end

VisRef = SR.FieldsToSet(Offset,ColVis,LabVis);
OccRef = SR.FieldsToSet(Offset,ColOcc,LabOcc);

end