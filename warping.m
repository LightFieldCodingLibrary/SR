function SRSet = warping(SRSet,varargin)
%LFTOSR Builds a super-ray set from a light-field
%   LFTOSR(LFSet,interpMethod,dispComp,maskValues)

%% Set parameter values according to given arguments
interps = {'nearest','linear','cubic','spline'};
boundaryTypes = {'current','any','all'};

p = inputParser; p.KeepUnmatched = true; p.StructExpand = true;
p.addRequired ('dispX', @(x) ~isempty(x));
p.addRequired ('dispY', @(x) ~isempty(x));
p.addParameter('interpMethod', 'nearest', @(x) any(validatestring(x,interps)));
p.addParameter('dispComp'    , false    , @islogical);
p.addParameter('SRBoundary'  , 'current', @(x) any(validatestring(x,boundaryTypes)));
p.addParameter('LFBoundary'  , true     , @islogical);
p.addParameter('relative'    , true     , @islogical);

p.parse(varargin{:});
dispX = p.Results.dispX;
dispY = p.Results.dispY;
interpMethod = p.Results.interpMethod;
dispComp = p.Results.dispComp;
SRBoundary = p.Results.SRBoundary;
LFBoundary = p.Results.LFBoundary;
relative = p.Results.relative;

if isnumeric(dispX)
    dispX = num2cell(dispX);
    relative = true;
end

if isnumeric(dispY)
    dispY = num2cell(dispY);
    relative = true;
end

%% Initialize constants and interpolants
% Lightfield properties
LFVal  = LFSet.Value;
LFLab  = LFSet.Label;
LFDispX = LFSet.DisparityX;
LFDispY = LFSet.DisparityY;
LFPos  = LFSet.Position;
LFSize = size(LFVal);

% Number of super-rays to be in the set
numLab = max(LFLab(:));

% Initialize super-ray set
[SRVal,SRLab,SRPos,SRDispX,SRDispY] = deal(cell(numLab,1));
SRSet = SR.FieldsToSet(SRVal,SRLab,SRPos,SRDispX,SRDispY);

% Lightfield grid vectors representing super-ray bounding box
ugv = reshape(1:LFSize(1),[],1);
vgv = reshape(1:LFSize(2),1,[]);
xgv = reshape(1:LFSize(3),1,1,[]);
ygv = reshape(1:LFSize(4),1,1,1,[]);

% Lightfield (sample) coordinates (4D)
[~,~,LFX,LFY]   = ndgrid(ugv,vgv,xgv,ygv);
LFPosX = LFPos(3);
LFPosY = LFPos(4);

% Lightfield central (reference) view coordinates 
uref = floor(LFSize(1)/2)+1; 
vref = floor(LFSize(2)/2)+1; 

% Fill NaN values to allow cubic interpolation
LFVal = fillmissing(LFVal,'nearest',3);
LFVal = fillmissing(LFVal,'nearest',4);

% Check size of interpolated data (for GriddedInterpolant)
if numel(LFVal)==1, szVal=1; else, szVal = size(LFVal); end
singLFVal = szVal==1; repVal = double(singLFVal);
repVal(~singLFVal) = 1; repVal(singLFVal) = 3;

% Expand any singleton dimension (need at least 3)
LFValInt = LFVal;
if any(singLFVal),LFValInt = repmat(LFVal,repVal); end

% Check size of interpolated data (for GriddedInterpolant)
if numel(LFLab)==1, szLab=1; else, szLab = size(LFLab); end
singLFLab = szLab==1; repLab = double(singLFLab);
repLab(~singLFLab) = 1; repLab(singLFLab) = 3;

% Expand any singleton dimension (need at least 3)
LFLabInt = LFLab;
if any(singLFLab),LFLabInt = repmat(LFLab,repLab); end

% Initialize interpolants
ValInt = griddedInterpolant(LFValInt,interpMethod,'nearest'); % Interpolant on pixel values
LabInt = griddedInterpolant(LFLabInt,'nearest'   ,'nearest'); % Interpolant on pixel labels

disp(['Pixels: "' ValInt.Method '" interpolation, "' ValInt.ExtrapolationMethod '" extrapolation']);
disp(['Labels: "' LabInt.Method '" interpolation, "' LabInt.ExtrapolationMethod '" extrapolation']);

%% Build super-ray set
utils.printSameLine();
for lab = 1:numLab
    
    utils.printSameLine(['Constructing super-ray ' num2str(lab) '/' num2str(numLab)]);
    
    % Mask corresponding to lightfield pixels in current super-ray
    Mask = LFLab==lab;
    
    % Set current super-ray disparity
    SRSet(lab).DisparityX = median(LFDispX(Mask));    
    SRSet(lab).DisparityY = median(LFDispY(Mask));    

    % x and y coordinates of each pixel of the super-ray after disparity compensation
    if relative
        dispX{lab} = (ugv-uref).*dispX{lab};
        dispY{lab} = (vgv-vref).*dispY{lab};
    end
    
    projX = LFX;
    projY = LFY;
    
    if ~dispComp
        projX = projX-dispX{lab};
        projY = projY-dispY{lab};
    end
    
    % Min and Max x and y coordinates of the pixels of the super-ray
    minprojX = floor(min(projX(Mask))); maxprojX = ceil(max(projX(Mask)));
    minprojY = floor(min(projY(Mask))); maxprojY = ceil(max(projY(Mask)));
    
    % Set current super-ray position (offset from reference frame center)
    SRSet(lab).Position = [1,1,LFPosX+minprojX,LFPosY+minprojY]-1;
    
    % Super-ray grid vectors representing super-ray bounding box
    srugv = reshape(1:LFSize(1),[],1);
    srvgv = reshape(1:LFSize(2),1,[]);
    srxgv = minprojX:maxprojX;
    srygv = minprojY:maxprojY;
    
    % Super-ray (query) coordinates (4D)
    [SRUq,SRVq,SRXq,SRYq] = ndgrid(srugv,srvgv,srxgv,srygv);
    
    % Set current super-ray size (size of the bounding box)
    SRSize = [numel(srugv),numel(srvgv),numel(srxgv),numel(srygv)];
    
    % Perform disparity compensation if necessary
    if ~dispComp
        SRXq = SRXq+dispX{lab};
        SRYq = SRYq+dispY{lab};
    end
    
    % Interpolate super-ray from lightfield at query coordinates
    SRVal = ValInt(SRUq,SRVq,SRXq,SRYq);
    SRLab = LabInt(SRUq,SRVq,SRXq,SRYq);
    
    switch SRBoundary
        % Create mask corresponding to query points 
        case 'current'
            % inside super-ray boundary in current view
            inSRMask = SRLab==lab;
        case 'any'
            % inside super-ray boundary in any view
            inSRMask = SRLab==lab;
            inSRMask = repmat(any(any(inSRMask,1),2),size(inSRMask,1),size(inSRMask,2),1,1);
        case 'all'
            % inside super-ray bounding box
            inSRMask = true(SRSize);
    end
    
    % Set label inside super-ray boundary
    SRLab( inSRMask) = lab;
    
    % Remove values corresponding to query points outside mask
    SRVal(~inSRMask) = NaN;
    SRLab(~inSRMask) = 0;
    
    if LFBoundary
        % Mask corresponding to query points inside lightfield boundary
        inLFMask = SRXq>=0.5 & SRXq<=LFSize(3)+0.5 & ... % <==> round(Xq)>=1 & round(Xq)<=LFSize(3)
            SRYq>=0.5 & SRYq<=LFSize(4)+0.5;      %    & round(Yq)>=1 & round(Xq)<=LFSize(4)
        
        % Remove values corresponding to query points outside lightfield boundary
        SRVal(~inLFMask) = NaN;
        SRLab(~inLFMask) = 0;
    end
    
    % Set current super-ray pixel values and label
    SRSet(lab).Value = SRVal;
    SRSet(lab).Label = SRLab;
end
utils.printSameLine();

end