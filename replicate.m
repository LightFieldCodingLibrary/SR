function Vals = replicate(Vals,ResOff)
%REPLICATE Summary of this function goes here
%   Detailed explanation goes here

numVal = numel(Vals);

for val = 1:numVal
    Vals{val} = LF.replicate(Vals{val},ResOff);
end

end