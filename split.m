function Set = split(Set)
%SPLIT Summary of this function goes here
%   Detailed explanation goes here

[Color,Label,Offset] = utils.split(Set.Color,Set.Label,Set.Offset);

for lab = 1:numel(Label)
    if ~isempty(Label{lab})
        Color{lab}(Label{lab}~=lab) = nan;
    end
end

Set = SR.FieldsToSet(Offset,Color,Label);

end