function LFVis = merge(SRVis,LabRep)
%MERGE Summary of this function goes here
%   Detailed explanation goes here

LFVis = zeros(size(LabRep));
numLab = max(LabRep(:));

for lab = 1:numLab
    curSR = SRVis{lab};
    SRMask = ~isnan(curSR);
    LFMask = LabRep==lab;
    LFVis(LFMask) = curSR(SRMask);
end
end

