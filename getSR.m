function [SRRef,SRDisp] = getSR(LFRef,SRXq,SRYq,Method,LFDisp)
%GETSR Summary of this function goes here
%   Detailed explanation goes here

%% Initialize constants and interpolants
% Lightfield properties
LFSize = size(LFRef.Color);
ImgSize = LFSize(1:2);
numChan = LFSize(3);
ImgRes = LFSize(4:end);
Offset = LFRef.Offset;
Color  = LFRef.Color;
Label  = LFRef.Label;
centerView = floor(ImgRes/2)+1;
numView = prod(ImgRes);
numLab = numel(SRXq);

% Initialize super-ray fields
[SROff,SRCol,SRLab,SRDisp] = deal(cell(numLab,1));

% Initialize disparity if unavailable
if ~exist('LFDisp','var'); LFDisp = zeros(LFSize); end

% Pad values
Color = padarray(Color,[1,1,0,0,0],nan,'both');
Label = padarray(Label,[1,1,0,0,0],nan,'both');
LFDisp = padarray(LFDisp,[1,1,0,0,0],nan,'both');
ImgSize = ImgSize+2;
Offset(1:2) = Offset(1:2)-1;

seh = strel('arbitrary',[1,0,1]);
sev = strel('arbitrary',[1,0,1]');

Color = utils.fillmissing(Color,seh,1);
Label = utils.fillmissing(Label,seh,1);
LFDisp = utils.fillmissing(LFDisp,seh,1);

Color = utils.fillmissing(Color,sev,1);
Label = utils.fillmissing(Label,sev,1);
LFDisp = utils.fillmissing(LFDisp,sev,1);

% Replace missing values by zero (to avoid interpolation errors)
Color(isnan(Color))=0;
Label(isnan(Label))=0;
LFDisp(isnan(LFDisp))=0;

% Compute projected coordinates of reference samples
gv = arrayfun(@(x) 1:x,ImgSize,'UniformOutput',false);

[LFX,LFY] = ndgrid(gv{1:2});
LFX = LFX + Offset(1);
LFY = LFY + Offset(2);

progress('',0);

wgriddata = warning('query','MATLAB:griddata:DuplicateDataPoints');
wscattered = warning('query','MATLAB:scatteredInterpolant:DupPtsAvValuesWarnId');
warning('off','MATLAB:griddata:DuplicateDataPoints');
warning('off','MATLAB:scatteredInterpolant:DupPtsAvValuesWarnId');

%% Initialize super-rays
fprintf('Super-ray computation...\n');
for lab = 1:numLab
    % Compute super-ray grid boundary
    SRXc = SRXq{lab}(:,:,1,centerView(1),centerView(2));
    SRYc = SRYq{lab}(:,:,1,centerView(1),centerView(2));
    
    Mask = ~(isnan(SRXc)|isnan(SRYc));
    [~,mgv] = utils.tighten(Mask);
    
    srxqmin = SRXc(mgv{1}(1),mgv{2}(1)) - mgv{1}(1);
    sryqmin = SRYc(mgv{1}(1),mgv{2}(1)) - mgv{2}(1);
    
    SROff{lab} = [srxqmin,sryqmin,0,0,0];
    
    % Initialize super-ray color, label and disparities
    size_lab = size(SRXq{lab});
    size_lab_col = size_lab;
    size_lab_col(3) = numChan;
    
    SRCol {lab} = nan(size_lab_col);
    SRLab {lab} = nan(size_lab);
    SRDisp{lab} = nan(size_lab);
    
    % Compute super-ray view by view
    for v = 1:numView
        msg = ['Computing super-ray ',num2str(lab),'/',num2str(numLab),...
            ' from light field (view ',num2str(v),'/',num2str(numView),')\n'];
        progress(msg);
        
        Xq = SRXq{lab}(:,:,:,v);
        Yq = SRYq{lab}(:,:,:,v);
        
        % Reduce number of reference points for speed
        Mask = ...true(size(LFX(:,:,v)));
            LFX>=min(Xq(:))-1 & LFX<=max(Xq(:))+1 & ...
            LFY>=min(Yq(:))-1 & LFY<=max(Yq(:))+1;
        
        [~,mgv] = utils.tighten(Mask);
        
        LFXsub = LFX(mgv{:});
        LFYsub = LFY(mgv{:});
        
        M = isnan(Xq)|isnan(Yq);
        
        Xq(M) = 1;
        Yq(M) = 1;
        
        % Interpolate color
        for c = 1:numChan
            Colsub = Color(mgv{:},c,v);
            temp = interp2(LFYsub,LFXsub,Colsub,Yq,Xq,Method);
            temp(M) = nan;
            SRCol{lab}(:,:,c,v) = temp;
        end
        
        % Interpolate label
        Labelsub = Label(mgv{:},v);
        temp = interp2(LFYsub,LFXsub,Labelsub,Yq,Xq,'nearest');
        temp(M) = nan;
        Lab = temp;
        
        % Interpolate disparity
        LFDispsub = LFDisp(mgv{:},v);
        temp = interp2(LFYsub,LFXsub,LFDispsub,Yq,Xq,Method);
        temp(M) = nan;
        SRDisp{lab}(:,:,:,v) = temp;
        
        % Replace out of boundary values in label using cubic interpolation
        NaNLab = isnan(interp2(LFYsub,LFXsub,Labelsub,Yq,Xq,'cubic'));
        Lab(NaNLab) = nan;
        SRLab{lab}(:,:,:,v) = Lab;
    end
end

progress('',0);
fprintf('\n');

warning(wgriddata.state,'MATLAB:griddata:DuplicateDataPoints');
warning(wscattered.state,'MATLAB:scatteredInterpolant:DupPtsAvValuesWarnId');

% Create super-ray structure from fields
SRRef = SR.FieldsToSet(SROff,SRCol,SRLab);

    function progress(msg,varargin)
        persistent sz
        if isempty(sz); sz = 0; end
        if nargin>1; sz = varargin{1}; end
        
        fprintf(repmat('\b',1,sz));
        fprintf(msg);
        sz = numel(msg)-count(msg,'\n');
    end
end