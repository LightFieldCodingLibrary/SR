function [Ref,C,Value,U,S,V] = factorize(Ref,method,k,varargin)
%COMPLETESET Summary of this function goes here
%   Detailed explanation goes here

[Offset,Color,Label] = SR.SetToFields(Ref);
ColSize = cellfun(@size,Color,'UniformOutput',false);
LabSize = cellfun(@size,Label,'UniformOutput',false);
RefSize = size(Ref);
numLab = numel(Label);
numDims = ndims(Color{1});
colDims = [];
rowDims = [];

p = inputParser; p.StructExpand = true; p.KeepUnmatched = true;
p.addParameter('colDims', colDims, @isnumeric);
p.addParameter('rowDims', rowDims, @isnumeric);

p.parse(varargin{:});
colDims = p.Results.colDims;
rowDims = p.Results.rowDims;

if (isempty(colDims)&&isempty(rowDims)); colDims = 1:min(3,numDims); end
if isempty(colDims); colDims = setdiff(1:numDims,rowDims); end
if isempty(rowDims); rowDims = setdiff(1:numDims,colDims); end

[ColImgSize,LabImgSize] = deal(cell(1,numLab));
[ColImgSize(:),LabImgSize(:)] = deal({nan(1,numel(colDims))});

for lab = 1:numLab
    ColImgSize{lab} = ColSize{lab}(colDims);
    LabImgSize{lab} = LabSize{lab}(colDims);
    
    LabelMask  = repmat(Label{lab},size(Color{lab})./size(Label{lab}));
    LabelMask  = permute(LabelMask ,[colDims,rowDims]);
    Label{lab} = permute(Label{lab},[colDims,rowDims]);
    Color{lab} = permute(Color{lab},[colDims,rowDims]);
    LabelMask  = reshape(LabelMask ,prod(ColImgSize{lab}),[]);
    Label{lab} = reshape(Label{lab},prod(LabImgSize{lab}),[]);
    Color{lab} = reshape(Color{lab},prod(ColImgSize{lab}),[]);
    LabelMask  = any(LabelMask==0,2);
    Color{lab}(LabelMask,:) = nan;
end

switch method
    case 'global'
        Value = {Set2Matrix(Color)};
    case 'local'
        Value = Color;
end

[C,U,S,V] = deal(cell(RefSize));

for it = 1:numel(Value)
    [Value{it},C{it},U{it},S{it},V{it}] = utils.factorize(Value{it},k);
end

ImgRes(:) = deal({[k,1]});

switch method
    case 'global'
        Color = Matrix2Set(Value{1},ColImgSize,ImgRes);
        C = C{1};
    case 'local'
        Color = Value;
end

for lab = 1:numLab
    Color{lab} = reshape(Color{lab},[ColImgSize{lab},k]);
    Label{lab} = repmat(Label{lab}(:,1),1,k);
    Label{lab} = reshape(Label{lab},[LabImgSize{lab},k]);
    Color{lab} = ipermute(Color{lab},[colDims,rowDims]);
    Label{lab} = ipermute(Label{lab},[colDims,rowDims]);
end

Color = reshape(Color,RefSize);

Ref = SR.FieldsToSet(Offset,Color,Label);

%% Auxiliary functions
    function Matrix = Set2Matrix(Set)
        Matrix = cell2mat(reshape(Set,[],1));
    end
       
    function Set = Matrix2Set(Matrix,ImgSize,ImgRes)
        for iter = 1:numel(ImgSize)
            ImgSize{iter} = prod(ImgSize{iter});
        end
        
        ImgSize = cell2mat(ImgSize);
        ImgRes = prod(ImgRes{1});
        
        Set = mat2cell(Matrix,ImgSize,ImgRes);
    end
end