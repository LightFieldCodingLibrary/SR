function [LFRef,LFDisp] = getLF_parfor(SRRef,SRDisp,SRX,SRY,lfxgv,lfygv,Method)
%GETLF_PARFOR Summary of this function goes here
%   Detailed explanation goes here

%%
SRCol = {SRRef.Color};
SRLab = {SRRef.Label};

seh = strel('arbitrary',[1,0,1]);
sev = strel('arbitrary',[1,0,1]');

%% Initialize constants and interpolants
numChan = size(SRCol{1},3);
ImgSize = [numel(lfxgv),numel(lfygv),numChan];
ImgRes = size(SRRef(1).Color);
ImgRes = ImgRes(4:end);
Offset = [min(lfxgv),min(lfygv),1,1,1]-1;
numLab = numel(SRCol);
numView = prod(ImgRes);

Color = nan([ImgSize(1:2),numChan,ImgRes]);
Label = zeros([ImgSize(1:2),1,ImgRes]);
LFDisp = -inf([ImgSize(1:2),1,ImgRes]);

progress('',0);

wgriddata = warning('query','MATLAB:griddata:DuplicateDataPoints');
wscattered = warning('query','MATLAB:scatteredInterpolant:DupPtsAvValuesWarnId');
warning('off','MATLAB:griddata:DuplicateDataPoints');
warning('off','MATLAB:scatteredInterpolant:DupPtsAvValuesWarnId');

%% Compute light field using interpolation
% Compute light field super-ray by super-ray
for lab = 1:numLab
    msg = ['Computing light field from super-ray ',num2str(lab),'/',num2str(numLab),'\n'];
    progress(msg);
    
    % Replace missing values by zero (to avoid interpolation errors)
    for it = 1:2
        SRCol{lab} = utils.fillmissing(SRCol{lab},seh,1);
        SRDisp{lab} = utils.fillmissing(SRDisp{lab},seh,1);
        
        SRCol{lab} = utils.fillmissing(SRCol{lab},sev,1);
        SRDisp{lab} = utils.fillmissing(SRDisp{lab},sev,1);
    end
    
    SRCol{lab}(isnan(SRCol{lab})) = 0;
    SRLab{lab}(isnan(SRLab{lab})) = 0;
    SRDisp{lab}(isnan(SRDisp{lab})) = 0;
    
    SRCol_lab = SRCol{lab};
    SRLab_lab = SRLab{lab};
    SRDisp_lab = SRDisp{lab};
    SRX_lab = SRX{lab};
    SRY_lab = SRY{lab};
    
    % Compute super-ray view by view
    parfor v = 1:numView
        SRXsub    = SRX_lab   (:,:,1,v);
        SRYsub    = SRY_lab   (:,:,1,v);
        SRColsub  = SRCol_lab (:,:,:,v);
        SRLabsub  = SRLab_lab (:,:,1,v);
        SRDispsub = SRDisp_lab(:,:,1,v);
        
        SRColsub  = utils.fillmissing(SRColsub,seh,1);
        SRLabsub  = utils.fillmissing(SRLabsub,seh,1);
        SRDispsub = utils.fillmissing(SRDispsub,seh,1);
        
        SRColsub  = utils.fillmissing(SRColsub,sev,1);
        SRLabsub  = utils.fillmissing(SRLabsub,sev,1);
        SRDispsub = utils.fillmissing(SRDispsub,sev,1);
        
        % Compute super-ray boundary
        srxqmin = ceil (min(SRXsub(:)));
        srxqmax = floor(max(SRXsub(:)));
        sryqmin = ceil (min(SRYsub(:)));
        sryqmax = floor(max(SRYsub(:)));
        
        % Reduce number of query points for speed
        xgv = lfxgv>=srxqmin & lfxgv<=srxqmax;
        ygv = lfygv>=sryqmin & lfygv<=sryqmax;
        
        [LFXsub,LFYsub] = ndgrid(lfxgv(xgv),lfygv(ygv));
        
        [SRXsub,SRYsub,LFXsub,LFYsub] = utils.gridCoords(SRXsub,SRYsub,LFXsub,LFYsub);
        
        queryNaN = isnan(LFXsub)|isnan(LFYsub);
        
        LFXsub(queryNaN) = 1;
        LFYsub(queryNaN) = 1;
        
        % Interpolate disparity
        tempDisp = interp2(SRYsub,SRXsub,SRDispsub,LFYsub,LFXsub,Method);
        
        % Interpolate label
        tempLab = interp2(SRYsub,SRXsub,SRLabsub,LFYsub,LFXsub,'nearest');
        
        % Find out of boundary values in label using cubic interpolation
        LabNaN = isnan(interp2(SRYsub,SRXsub,SRLabsub,LFYsub,LFXsub,'cubic'));
        
        LFDisp_ = LFDisp(:,:,:,v);
        Label_  = Label (:,:,:,v);
        Color_  = Color (:,:,:,v);
        
        % Combined depth + stencil buffer
        depthStencilMask = tempDisp>LFDisp_(xgv,ygv,:,:) & tempLab==lab;
        combinedMask = ~(queryNaN|LabNaN)&depthStencilMask;
        
        buffDisp = LFDisp_(xgv,ygv,:,:);
        buffLab  = Label_ (xgv,ygv,:,:);
        
        buffDisp(combinedMask) = tempDisp(combinedMask);
        buffLab (combinedMask) = tempLab (combinedMask);
        
        LFDisp_(xgv,ygv,:,:) = buffDisp;
        Label_ (xgv,ygv,:,:) = buffLab;
        
        % Interpolate color
        for c = 1:numChan
            buffCol = Color_(xgv,ygv,c,:);
            tempCol = interp2(SRYsub,SRXsub,SRColsub(:,:,c),LFYsub,LFXsub,Method);
            buffCol(combinedMask) = tempCol(combinedMask);
            Color_(xgv,ygv,c,:) = buffCol;
        end
        
        LFDisp(:,:,:,v) = LFDisp_;
        Label (:,:,:,v) = Label_;
        Color (:,:,:,v) = Color_;
    end
end

progress('',0);
fprintf('\n');

warning(wgriddata.state,'MATLAB:griddata:DuplicateDataPoints');
warning(wscattered.state,'MATLAB:scatteredInterpolant:DupPtsAvValuesWarnId');

LFRef = SR.FieldsToSet(Offset,Color,Label);

    function progress(msg,varargin)
        persistent sz
        if isempty(sz); sz = 0; end
        if nargin>1; sz = varargin{1}; end
        
        fprintf(repmat('\b',1,sz));
        fprintf(msg);
        sz = numel(msg)-count(msg,'\n');
    end
end