function SRSet = warpSet(SRSet,varargin)
%WARPSET Summary of this function goes here
%   Detailed explanation goes here

[Val,Lab,Pos,DispX,DispY] = SR.SetToFields(SRSet);

for it = 1:numel(Lab)
    Lab{it} = Lab{it}==it;
end

for it = 1:numel(Val)
    [Val{it},Lab{it},Pos{it},DispX{it},DispY{it}] = SR.warp(...
        Val{it},Lab{it},Pos{it},DispX{it},DispY{it},varargin{:});
end

for it = 1:numel(Lab)
    Lab{it} = it*double(Lab{it});
end

SRSet = SR.FieldsToSet(Val,Lab,Pos,DispX,DispY);
end