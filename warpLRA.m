function [PSNRin,PSNRout,M,MMask,MPos,MDispX,MDispY,...
    projM,projValue,projMask,projPos,projDispX,projDispY]...
    = warpLRA(Value,Mask,Pos,DispX,DispY,k,varargin)
%WARPLRA Warping low rank approximation
%   Compute disparity update that best matches low-rank approximation

%% Parse input
mins = {'original','rank'};
p = inputParser; p.KeepUnmatched = true; p.StructExpand = true;
p.addParameter('keepCompleted', true      , @islogical);
p.addParameter('minMethod'    , 'original', @(x) any(validatestring(x,mins)));

p.parse(varargin{:});
keepCompleted = p.Results.keepCompleted;
minMethod = p.Results.minMethod;

%% Forward transform of original values
[projValue,projMask,projPos,projDispX,projDispY,initMask] = SR.warp(...
    Value,Mask,Pos,DispX,DispY,varargin{:},'direction','forward');

%%
switch minMethod
    case 'original'
        projValue(~initMask) = nan;
    case 'rank'
        projValue(~projMask) = nan; %Allow rank completion of unknown pixels
end

if keepCompleted
    projMask = repmat(any(any(projMask,1),2),size(projMask,1),size(projMask,2),1,1);
end

%% Constants
projSize = size(projValue);

%% Remove unknown entries
projValue(~projMask) = nan;

%% Complete before low-rank approximation
projValue = SR.complete(projValue,'rank');

%% Compute low-rank approximation
[B,C] = SR.factorize(projValue);
BMat = LF.LFToMat(B);
MMat = BMat(:,1:k)*C(1:k,:);

projM = LF.MatToLF(MMat,projSize);

%% Remove unknown entries after low-rank approximation
projValue(~projMask) = nan;
projM(~projMask) = nan;

%% Compute inner PSNR
PSNRin = psnr(projM(projMask),projValue(projMask));

%% Backward transform of low-rank approximation
[M,MMask,MPos,MDispX,MDispY] = SR.warp(...
    projM,projMask,projPos,projDispX,projDispY,varargin{:},'direction','backward');

%% Align original values, low-rank approximation and mask
[Value_,~,M_,MPos_] = align(Value,Pos,M,MPos);
MMask_ = align(MMask,MPos,M_,MPos_);
ValueMask_ = ~isnan(Value_);

%% Mask corresponding to known and reprojected entries
Mask_ = ValueMask_&MMask_;

%% Compute outer PSNR
PSNRout = psnr(M_(Mask_),Value_(Mask_));
end

function [Val,ValPos,Mask,MaskPos] = align(Val,ValPos,Mask,MaskPos)
ValSize = size(Val);
MaskSize = size(Mask);

ValEnd = ValPos +ValSize;
MaskEnd = MaskPos+MaskSize;

minPos = min(ValPos,MaskPos);
maxPos = max(ValEnd,MaskEnd);

if islogical(Val)
    Val = padarray(Val,ValPos-minPos,false,'pre');
    Val = padarray(Val,maxPos-ValEnd,false,'post');
else
    Val = padarray(Val,ValPos-minPos,nan,'pre');
    Val = padarray(Val,maxPos-ValEnd,nan,'post');
end

if islogical(Mask)
    Mask = padarray(Mask,MaskPos-minPos,false,'pre');
    Mask = padarray(Mask,maxPos-MaskEnd,false,'post');
else
    Mask = padarray(Mask,MaskPos-minPos,nan,'pre');
    Mask = padarray(Mask,maxPos-MaskEnd,nan,'post');
end

[ValPos,MaskPos] = deal(minPos);
end
