function Ref = join(Ref)
%JOIN Summary of this function goes here
%   Detailed explanation goes here

[Offset,Color,Label] = SR.SetToFields(Ref);

[Label,Offset,Color] = utils.join(Label,Offset,Color);

Ref = SR.FieldsToSet(Offset,Color,Label);

end