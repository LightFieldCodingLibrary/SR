function C = project(Ref,Rec,method)
%COMPLETESET Summary of this function goes here
%   Detailed explanation goes here

[Ref,Rec] = SR.align(Ref,Rec);

[~,ColorRef,LabelRef] = SR.SetToFields(Ref);
[~,ColorRec,LabelRec] = SR.SetToFields(Rec);

Size = size(Ref);
numLab = numel(LabelRef);

for lab = 1:numLab
    SizeRef = size(ColorRef{lab});
    SizeRef(end+1:5) = 1;
    ImgSizeRef = SizeRef(1:3);
    
    M = (LabelRef{lab}~=lab) & true(SizeRef);
    ColorRef{lab}(M) = 0;
    ColorRef{lab} = reshape(ColorRef{lab},prod(ImgSizeRef),[]);
    
    SizeRec = size(ColorRec{lab});
    SizeRec(end+1:5) = 1;
    ImgSizeRec = SizeRec(1:3);
    
    M = (LabelRec{lab}~=lab) & true(SizeRec);
    ColorRec{lab}(M) = 0;
    ColorRec{lab} = reshape(ColorRec{lab},prod(ImgSizeRec),[]);
end

switch method
    case 'global'
        ValueRef = {Set2Matrix(ColorRef)};
        ValueRec = {Set2Matrix(ColorRec)};
    case 'local'
        ValueRef = ColorRef;
        ValueRec = ColorRec;
end

C = cell(Size);

for it = 1:numel(ValueRec)
    C{it} = pinv(ValueRec{it})*ValueRef{it};
end

switch method
    case 'global'
        C = C{1};
end

%% Auxiliary functions
    function Matrix = Set2Matrix(Set)
        Matrix = cell2mat(reshape(Set,[],1));
    end
end