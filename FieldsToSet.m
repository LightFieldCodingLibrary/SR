function SRSet = FieldsToSet(Offset,Color,Label)
%FieldsToSet Create a super-ray set struct given its fields
%   ImgSize:    Image size
%   ImgRes:     Angular size
%   Offset:     Offset from origin
%   Color:      RGB or Grayscale pixel values
%   Label:      Label

SRSet = struct('Offset',Offset,'Color',Color,'Label',Label);
end