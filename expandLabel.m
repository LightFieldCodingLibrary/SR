function Ref = expandLabel(Ref,se)
%EXPANDLABEL Summary of this function goes here
%   Detailed explanation goes here

[Offset,Color,Label] = SR.SetToFields(Ref);

numLab = numel(Label);

for lab = 1:numLab
    M = imdilate(Label{lab}==lab,se) & Label{lab}~=0;% & ~isnan(Label{lab});
    Label{lab}(M) = lab;
end

Ref = SR.FieldsToSet(Offset,Color,Label);

end