function [SRXq,SRYq,SRDispX,SRDispY] = get_affine_query_coordinates(Label,lfgv,dispParams)
%GET_AFFINE_QUERY_COORDINATES Summary of this function goes here
%   Detailed explanation goes here
%%

%% Initialize constants and interpolants
% Lightfield properties
numLab = max(Label(:));
lfgv{3} = 1;
ugv = reshape(lfgv{4},1,1,1,[],1);
vgv = reshape(lfgv{5},1,1,1,1,[]);
[X,Y,~,U,V] = ndgrid(lfgv{:});
[SRXq,SRYq,SRDispX,SRDispY] = deal(cell(1,numLab));

% Replace missing values by zero (to avoid interpolation errors)
Label(isnan(Label))=0;

% Remove entries corresponding to missing labels in accordance to loadReference
ind = ~cellfun(@isempty,dispParams);
dispParams = dispParams(ind);

%%
fprintf('Super-ray sampling coordinates computation...\n');
progress('',0);
for lab = 1:numLab
    msg = ['Initializing super-ray ',num2str(lab),'/',num2str(numLab),'\n'];
    progress(msg);
    
    M = Label==lab;
    
    a = dispParams{lab}(1);
    b = dispParams{lab}(2);
    c = dispParams{lab}(3);
    
    if numel(dispParams{lab})>=6
        d = dispParams{lab}(4);
        e = dispParams{lab}(5);
        f = dispParams{lab}(6);
    else
        d = a;
        e = b;
        f = c;
    end
    
    X_ = X(M(:));
    Y_ = Y(M(:));
    U_ = U(M(:));
    V_ = V(M(:));
    
    tX = X_-U_.*(a.*X_+b.*Y_+c)./( 1+a.*U_+b.*V_);
    tY = Y_-V_.*(d.*X_+e.*Y_+f)./( 1+d.*U_+e.*V_);
    
    srxgv = floor(min(tX(:))):ceil(max(tX(:)));
    srygv = floor(min(tY(:))):ceil(max(tY(:)));
    
    [X_,Y_] = ndgrid(srxgv,srygv);
    Dx = a.*X_+b.*Y_+c;
    Dx = reshape(Dx,size(X_));
    Dy = d.*X_+e.*Y_+f;
    Dy = reshape(Dy,size(X_));
    
    [SRXq{lab},SRYq{lab}] = ndgrid(srxgv,srygv,lfgv{3:end});
    SRXq{lab} = SRXq{lab}+ugv.*Dx;
    SRYq{lab} = SRYq{lab}+vgv.*Dy;
    
    SRDispX{lab} = ones(size(SRXq{lab})).*Dx;
    SRDispY{lab} = ones(size(SRXq{lab})).*Dy;
end

progress('',0);
fprintf('\n');

    function progress(msg,varargin)
        persistent sz
        if isempty(sz); sz = 0; end
        if nargin>1; sz = varargin{1}; end
        
        fprintf(repmat('\b',1,sz));
        fprintf(msg);
        sz = numel(msg)-count(msg,'\n');
    end
end