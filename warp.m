function [Value,Mask,Pos,dX,dY,initMask] = warp(Value,Mask,Pos,dX,dY,varargin)
%WARP Summary of this function goes here
%   Detailed explanation goes here

%% Set parameter values according to given arguments
interps = {'nearest','linear','cubic','spline'};
directions = {'forward','backward'};

p = inputParser; p.KeepUnmatched = true; p.StructExpand = true;
p.addParameter('interpMethod', 'cubic'  , @(x) any(validatestring(x,interps   )));
p.addParameter('direction'   , 'forward', @(x) any(validatestring(x,directions)));
p.addParameter('backproject' , true     , @islogical);

p.parse(varargin{:});
interpMethod = p.Results.interpMethod;
direction = p.Results.direction;
backproject = p.Results.backproject;

%% Compute reference grid
% Reference size
End = size(Value);

% Mask bounding box
gv = subarrayIdx(Mask);

% Mask grid coordinates
[~,~,X,Y] = ndgrid(gv{:});

% Mask grid vectors
[ugv,vgv,~,~] = deal(gv{:});

ugv = reshape(ugv,[],1);
vgv = reshape(vgv,1,[]);

% Reference view
u_ref = floor(numel(ugv)/2)+1;
v_ref = floor(numel(vgv)/2)+1;

% Disparity
DispX = dX*(ugv-u_ref);
DispY = dY*(vgv-v_ref);

if backproject
    % Temporary mask
    tempMask = Mask(gv{:});
    
    % Backproject coordinates
    SRX = X + DispX;
    SRY = Y + DispY;
    
    % Backprojected boundaries
    minSRX = floor(min(SRX(tempMask))); maxSRX = ceil(max(SRX(tempMask)));
    minSRY = floor(min(SRY(tempMask))); maxSRY = ceil(max(SRY(tempMask)));
    
    minSRX = max(minSRX,1); maxSRX = min(maxSRX,End(3));
    minSRY = max(minSRY,1); maxSRY = min(maxSRY,End(4));
    
    % Reference grid vectors
    xgv = minSRX:maxSRX;
    ygv = minSRY:maxSRY;
    
    % Reference bounding box
    gv = {ugv,vgv,xgv,ygv};
end

% Reference grid
[~,~,X,Y] = ndgrid(gv{:});

%% Crop reference
Value = Value(gv{:});
Mask  = Mask (gv{:});

Pos_ = cellfun(@(x) x(1),gv)-1;
End_ = cellfun(@(x) x(end),gv);

%% Compute query grid
% Project coordinates
SRX = X - DispX;
SRY = Y - DispY;

% Projected boundaries
minPosX = floor(min(SRX(Mask))); maxPosX = ceil(max(SRX(Mask)));
minPosY = floor(min(SRY(Mask))); maxPosY = ceil(max(SRY(Mask)));

% Query grid vectors
uqgv = ugv;
vqgv = vgv;
xqgv = minPosX:maxPosX;
yqgv = minPosY:maxPosY;

% Query bounding box
qgv = {uqgv,vqgv,xqgv,yqgv};

% Query grid
[Uq,Vq,Xq,Yq] = ndgrid(qgv{:});

Xq = Xq + DispX;
Yq = Yq + DispY;

% Use invertible grid coordinates to compute mask
switch direction
    case 'forward'
        Xq_Mask = floor(Xq+0.5); 
        Yq_Mask = floor(Yq+0.5); 
    case 'backward'
        Xq_Mask = ceil(Xq-0.5); 
        Yq_Mask = ceil(Yq-0.5); 
end

%% Interpolate
% Extend signal to allow cubic interpolation (way faster than fillmissing)
seAng = strel('arbitrary',[0,1,0]|[0,1,0]');
seSpa = strel('arbitrary',reshape([0,1,0]|[0,1,0]',1,1,3,3));

if strcmp(interpMethod,'cubic')&&any(isnan(Value(:)))
    Value = extend(Value,seSpa,6);
    Value = extend(Value,seAng,6);
end

% Expand any singleton dimension for interpolation (at least 3 for cubic)
szVal = size(Value); szVal(end+1:4)=1;

singDims = double(szVal==1);
if any(singDims)
    Value = repmat(Value,2*double(singDims)+1);
    for it = find(singDims)
        gv{it} = gv{it}-1:gv{it}+1;
    end
end

dualDims = double(szVal==2);
if any(dualDims)
    Value = repmat(Value,double(dualDims)+1);
    for it = find(dualDims)
        gv{it} = [gv{it},gv{it}(end)+1];
    end
    
end


% Cast mask as double for interpolation
Mask = double(Mask);

% Initialize interpolants
ValueInt = griddedInterpolant(gv,Value,interpMethod,'nearest');
MaskInt  = griddedInterpolant(gv,Mask ,'nearest'   ,'nearest');

% Interpolation
Value = ValueInt(Uq,Vq,Xq,Yq);
Mask  = MaskInt(Uq,Vq,Xq_Mask,Yq_Mask); Mask = logical(Mask);

% Mask of query coordinates inside initial and reference boundary
initMask = Xq_Mask>0       & Xq_Mask<End (3)+1 & Yq_Mask>0        & Yq_Mask<End (4)+1;
refMask  = Xq_Mask>Pos_(3) & Xq_Mask<End_(3)+1 & Yq_Mask>Pos_(4)  & Yq_Mask<End_(4)+1;

% New mask
Mask = Mask & refMask;

% New position
Pos = Pos + cellfun(@(x) x(1),qgv)-1;

% New disparities
dX = -dX;
dY = -dY;
end

%% Auxiliary functions
% Compute bounding box of mask
function gv = subarrayIdx(Mask)
s = regionprops(Mask,'SubarrayIdx');
if numel(s)==1
    gv = s.SubarrayIdx;
else
    gv = {s.SubarrayIdx};
    [ugv,vgv,xgv,ygv] = cellfun(@(x)deal(x{:}),gv,'UniformOutput',false);
    
    for it = 1:numel(s)-1
        ugv = [union(ugv{1},ugv{2}),ugv(3:end)];
        vgv = [union(vgv{1},vgv{2}),vgv(3:end)];
        xgv = [union(xgv{1},xgv{2}),xgv(3:end)];
        ygv = [union(ygv{1},ygv{2}),ygv(3:end)];
    end
    
    ugv = min(ugv{:}):max(ugv{:});
    vgv = min(vgv{:}):max(vgv{:});
    xgv = min(xgv{:}):max(xgv{:});
    ygv = min(ygv{:}):max(ygv{:});
    gv = {ugv,vgv,xgv,ygv};
end
end

% Extend signal
function Value = extend(Value,se,numIter)
for i=1:numIter
    Mask = isnan(Value);
    Temp = Value;
    Temp(Mask)=-Inf;
    Temp = imdilate(Temp,se);
    Temp(isinf(Temp))=NaN;
    Value(Mask) = Temp(Mask);
end
end