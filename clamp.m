function RefSet = clamp(RefSet,varargin)
%CLAMP Summary of this function goes here
%   Detailed explanation goes here

numLab = numel(RefSet);

for lab = 1:numLab
    RefSet(lab).Color = utils.clamp(RefSet(lab).Color,varargin{:});
end

end