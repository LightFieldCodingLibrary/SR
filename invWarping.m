function [LFSet,LFBuf] = SRtoLF(SRSet,LFSize,varargin)
%SRTOLF Build a lightfield from a super-ray set
%   SRTOLF(SRSet,LFSize,dispComp,extend,interpMethod)

%% Set parameter values according to given arguments
interps = {'nearest','linear','cubic','spline'};
orders = {'ascend','descend'};

p = inputParser; p.KeepUnmatched = true; p.StructExpand = true;
p.addRequired ('dispX', @(x) ~isempty(x)&numel(x)==numel(SRSet));
p.addRequired ('dispY', @(x) ~isempty(x)&numel(x)==numel(SRSet));
p.addParameter('interpMethod', 'nearest', @(x) any(validatestring(x,interps)));
p.addParameter('dispComp'    , false    , @islogical);
p.addParameter('extend'      , false    , @islogical);
p.addParameter('order'       , 'ascend' , @(x) any(validatestring(x,orders)));
p.addParameter('relative'    , true     , @islogical);

p.parse(varargin{:});
dispX = p.Results.dispX;
dispY = p.Results.dispY;
interpMethod = p.Results.interpMethod;
dispComp = p.Results.dispComp;
extend = p.Results.extend;
order = p.Results.order;
relative = p.Results.relative;

if isnumeric(dispX)
    dispXcell = num2cell(dispX);
    relative = true;
else
    dispXcell = dispX;
    dispX = cell2mat(dispX);
end

if isnumeric(dispY)
    dispYcell = num2cell(dispY);
    relative = true;
else
    dispYcell = dispY;
    dispY = cell2mat(dispY);
end

%% Initialize constants
SRVal  = {SRSet.Value};
% SRLab  = {SRSet.Label};
SRPos  = {SRSet.Position};
% SRDispX = {SRSet.DisparityX};
% SRDispY = {SRSet.DisparityY};
SRSize = cellfun(@size,SRVal,'UniformOutput',false);

% Number of super-rays in the set
numLab = numel(SRSet);

% Lightfield central (reference) view coordinates 
uref = floor(SRSize{1}(1)/2)+1; 
vref = floor(SRSize{1}(2)/2)+1; 

% Structuring element to dilate super-ray for cubic interpolation
se = strel('arbitrary',reshape([0,1,0],1,1,3)|reshape([0,1,0],1,1,1,3));

% Lightfield position wrt the reference frame
LFPos = [0,0,0,0];

if extend
    % Extend lightfield size so as to fit all super-ray pixels
    
    % Retrieve min and max x and y positions
    [XPos ,YPos ] = cellfun(@(x) deal(x(3),x(4)),SRPos );
    [XSize,YSize] = cellfun(@(x) deal(x(3),x(4)),SRSize);
    
    minXPos = min(XPos(:)); maxXPos = max(XPos(:)+XSize(:));
    minYPos = min(YPos(:)); maxYPos = max(YPos(:)+YSize(:));
    
    % Update lightfield position and size
    LFPos = [0,0,minXPos,minYPos];
    LFSize(3) = maxXPos-minXPos; LFSize(4) = maxYPos-minYPos;
end
    
% Initialize lightfield
[LFVal,LFLab,LFDispX,LFDispY,LFBuf] = deal(nan(LFSize));
LFSet = SR.FieldsToSet(LFVal,LFLab,LFPos,LFDispX,LFDispY);

switch order
    case 'ascend'
        disps = max([dispX;dispY],[],1);
    case 'descend'
        disps = min([dispX;dispY],[],1);
end

[~,sortedLab] = sort(disps,order);

%% Build lightfield
utils.printSameLine();
for lab = sortedLab 
    utils.printSameLine(['Projecting super-ray ' num2str(lab) '/' num2str(numLab)]);
    
    % Current super-ray properties
    SRVal  = SRSet(lab).Value;
    SRSize = size(SRVal);
    SRPos  = SRSet(lab).Position;
    SRDispX = SRSet(lab).DisparityX;
    SRDispY = SRSet(lab).DisparityY;
    SRLab  = SRSet(lab).Label;
    
    % Super-ray position (offset from reference frame center)
    SRx = SRPos(3);
    SRy = SRPos(4);
    
    % Lightfield position (offset from reference frame center)
    LFx = LFPos(3);
    LFy = LFPos(4);
    
    % Super-ray grid vectors representing super-ray bounding box
    ugv = reshape(1:SRSize(1),[],1);
    vgv = reshape(1:SRSize(2),1,[]);
    xgv = reshape(1:SRSize(3),1,1,[]);
    ygv = reshape(1:SRSize(4),1,1,1,[]);
    
    if relative
        dispXcell{lab} = (ugv-uref).*dispXcell{lab};
        dispYcell{lab} = (vgv-vref).*dispYcell{lab};
    end

    % Mask corresponding to query points inside super-ray boundary
    SRMask = ~isnan(SRVal);
    
    % Extend the signal for cubic interpolation 
    % Dilation in spatial dimensions
    for i=1:5
        SRDilMask = ~isnan(SRVal);
        temp = SRVal;
        temp(~SRDilMask)=-Inf;
        temp = imdilate(temp,se);
        temp(isinf(temp))=NaN;
        SRVal(~SRDilMask) = temp(~SRDilMask);
    end
    
    % Fill NaN values with zero in angular dimensions
    PadMask = ~isnan(SRVal);
    PadMaskComp = repmat(any(any(PadMask,1),2),size(PadMask,1),size(PadMask,2),1,1);
    SRVal(PadMaskComp&~PadMask) = 0;
    
    % Check size of interpolated data (for GriddedInterpolant) 
    if numel(SRVal)==1, szVal=1; else, szVal = size(SRVal); end 
    singSRVal = szVal==1; repVal = double(singSRVal); 
    repVal(~singSRVal) = 1; repVal(singSRVal) = 3; 
    
    % Expand any singleton dimension (need at least 3)
    if any(singSRVal),SRVal = repmat(SRVal,repVal); end
    
    % Check size of interpolated data (for GriddedInterpolant)
    if numel(SRLab)==1, szLab=1; else, szLab = size(SRLab); end
    singSRLab = szLab==1; repLab = double(singSRLab);
    repLab(~singSRLab) = 1; repLab(singSRLab) = 3;
    
    % Expand any singleton dimension (need at least 3)
    if any(singSRLab),SRLab = repmat(SRLab,repLab); end
    
    % Initialize interpolants
    ValInt = griddedInterpolant(SRVal,interpMethod,'nearest');
    LabInt = griddedInterpolant(SRLab,'nearest'   ,'nearest');
    
    % Super-ray (sample) coordinates (4D)
    [SRU,SRV,SRX,SRY] = ndgrid(ugv,vgv,xgv,ygv);
    SRX = SRx+SRX-LFx;
    SRY = SRy+SRY-LFy;
    
    % Perform disparity compensation if necessary
    if ~dispComp
        SRX = SRX+dispXcell{lab};
        SRY = SRY+dispYcell{lab};
    end
    
    % Super-ray (query) coordinates (4D)
    SRXq = round(SRX);
    SRYq = round(SRY);
    
    deltaX = SRXq-SRX;
    deltaY = SRYq-SRY;
    
    % Interpolate lightfield from super-ray at query coordinates
    LFVal = ValInt(SRU,SRV,xgv+deltaX,ygv+deltaY);
    LFLab = LabInt(SRU,SRV,xgv+deltaX,ygv+deltaY);
    
    % Remove values corresponding to query points outside super-ray boundary
    LFVal(~SRMask) = NaN;
    LFLab(~SRMask) = NaN;
    
    % Mask corresponding to query points inside lightfield boundary
    LFMask = SRXq>=0.5 & SRXq<=LFSize(3)+0.5 & ... % <==> round(Xq)>=1 & round(Xq)<=LFSize(3)
             SRYq>=0.5 & SRYq<=LFSize(4)+0.5;      %    & round(Yq)>=1 & round(Xq)<=LFSize(4)
    
    % Mask corresponding to query points inside super-ray and lightfield boundary
    SRLFMask = SRMask&LFMask;
    
    % Linear indices corresponding to query coordinates
    indLF = uvxy2ind(LFSize,SRU,SRV,SRXq,SRYq);
    indSRLFMask = indLF(SRLFMask);
    
    % Set lightfield pixels values, label and disparity corresponding to current super-ray
    LFSet.Value     (indSRLFMask) = LFVal(SRLFMask);
    LFSet.Label     (indSRLFMask) = LFLab(SRLFMask);
    LFSet.DisparityX(indSRLFMask) = SRDispX;
    LFSet.DisparityY(indSRLFMask) = SRDispY;
    LFBuf           (indSRLFMask) = LFBuf(indSRLFMask)+1;    
end
utils.printSameLine();

end

function ind = uvxy2ind(LFSize,indu,indv,indx,indy)
ind = indu   +LFSize(1)*(...
     (indv-1)+LFSize(2)*(...
     (indx-1)+LFSize(3)*(...
      indy-1             )));
end