function [Offset,Color,Label] = SetToFields(Set)
%SetToFields Get fields of a super-ray set struct
%   ImgSize:    Image size
%   ImgRes:     Angular
%   Offset:     Offset from origin
%   Color:      RGB or Grayscale pixel values
%   Label:      Distinct label for each super-ray

SetSize = size(Set);
Offset = reshape({Set.Offset},SetSize);
Color  = reshape({Set.Color },SetSize);
Label  = reshape({Set.Label },SetSize);
end