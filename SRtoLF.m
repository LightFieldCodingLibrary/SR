function [LFRef,LFDisp] = SRtoLF(SRRef,SRDisp,lfxgv,lfygv,Method)
%LFTOSR Builds a super-ray set from a light-field
%   SRTOLF(LFSet)

%%
SRCol = {SRRef.Color};
SRLab = {SRRef.Label};
SROffset = {SRRef.Offset};
SRSize = cellfun(@size,{SRRef.Color},'UniformOutput',false);
[SRImgSize,SRImgRes] = cellfun(@(x) deal(x(1:3),x(4:end)),SRSize,...
    'UniformOutput',false);

%% Initialize constants and interpolants
numChan = size(SRCol{1},3);
ImgSize = [numel(lfxgv),numel(lfygv),numChan];
ImgRes = SRImgRes{1};
Offset = [min(lfxgv),min(lfygv),1,1,1]-1;
numLab = numel(SRCol);
numView = prod(ImgRes);

[LFXq,LFYq] = ndgrid(lfxgv,lfygv);
[SRX,SRY] = deal(cell(numLab,1));

Color = nan([ImgSize(1:2),numChan,ImgRes]);
Label = zeros([ImgSize(1:2),1,ImgRes]);
LFDisp = -inf([ImgSize(1:2),1,ImgRes]);

progress('',0);

%% Compute super-ray coordinates
for lab = 1:numLab
    msg = ['Initializing super-ray ',num2str(lab),'/',num2str(numLab),'\n'];
    progress(msg);
    
    % Compute grid coordinates
    xgv = SROffset{lab}(1)+(1:SRImgSize{lab}(1));
    ygv = SROffset{lab}(2)+(1:SRImgSize{lab}(2));
    ugv = 1:SRImgRes{lab}(1);
    ugv = ugv-floor(ugv(end)/2)-1;
    vgv = 1:SRImgRes{lab}(2);
    vgv = vgv-floor(vgv(end)/2)-1;
    
    % Reshape grid coordinate along corresponding dimension
    xgv = reshape(xgv,[],1,1,1);
    ygv = reshape(ygv,1,[],1,1);
    ugv = reshape(ugv,1,1,[],1);
    vgv = reshape(vgv,1,1,1,[]);
    
    % Compute projected coordinates of reference samples
    Disp = reshape(SRDisp{lab},[SRImgSize{lab}(1:2),SRImgRes{lab}]);
    SRX{lab} = xgv+ugv.*Disp.*ones(size(ygv)).*ones(size(vgv));
    SRY{lab} = ygv+vgv.*Disp.*ones(size(xgv)).*ones(size(ugv));
    
    % Replace missing values by zero (to avoid interpolation errors)
    SRLab{lab}(isnan(SRLab{lab})) = 0;
    SRDisp{lab}(isnan(SRDisp{lab})) = 0;
    SRCol{lab}(isnan(SRCol{lab})) = 0;
end
fprintf('\n');

progress('',0);

wgriddata = warning('query','MATLAB:griddata:DuplicateDataPoints');
wscattered = warning('query','MATLAB:scatteredInterpolant:DupPtsAvValuesWarnId');
warning('off','MATLAB:griddata:DuplicateDataPoints');
warning('off','MATLAB:scatteredInterpolant:DupPtsAvValuesWarnId');

%% Compute light field using interpolation
% Compute light field super-ray by super-ray
for lab = 1:numLab
    % Compute super-ray boundary
    srxqmin = floor(min(SRX{lab}(:)));
    srxqmax = ceil (max(SRX{lab}(:)));
    sryqmin = floor(min(SRY{lab}(:)));
    sryqmax = ceil (max(SRY{lab}(:)));
    
    % Reduce number of query points for speed
    xgv = lfxgv>=srxqmin & lfxgv<=srxqmax;
    ygv = lfygv>=sryqmin & lfygv<=sryqmax;
    
    % Compute super-ray view by view
    for v = 1:numView
        msg = ['Constructing super-ray ',num2str(lab),'/',num2str(numLab),...
            '\nView ',num2str(v),'/',num2str(numView),'\n'];
        progress(msg);
        
        SRXSub    = [];
        SRYSub    = [];
        SRLabSub  = [];
        SRDispSub = [];
        SRColSub  = [];
        
        for subLab = 1:numLab
            % Remove missing reference data
            SRXSubLab    = SRX   {subLab}(:,:,v);
            SRYSubLab    = SRY   {subLab}(:,:,v);
            SRLabSubLab  = SRLab {subLab}(:,:,v);
            SRDispSubLab = SRDisp{subLab}(:,:,v);
            SRColSubLab  = SRCol {subLab}(:,:,:,v);
            
            SRXSubLab    = reshape(SRXSubLab   ,[],1);
            SRYSubLab    = reshape(SRYSubLab   ,[],1);
            SRLabSubLab  = reshape(SRLabSubLab ,[],1);
            SRDispSubLab = reshape(SRDispSubLab,[],1);
            SRColSubLab  = reshape(SRColSubLab ,[],numChan);

            inMask = ...
                SRXSubLab>=srxqmin-1 & SRXSubLab<=srxqmax+1 &...
                SRYSubLab>=sryqmin-1 & SRYSubLab<=sryqmax+1;

            SRXSub    = cat(1,SRXSub   ,SRXSubLab   (inMask,:));
            SRYSub    = cat(1,SRYSub   ,SRYSubLab   (inMask,:));
            SRLabSub  = cat(1,SRLabSub ,SRLabSubLab (inMask,:));
            SRDispSub = cat(1,SRDispSub,SRDispSubLab(inMask,:));
            SRColSub  = cat(1,SRColSub ,SRColSubLab (inMask,:));
        end
        
        % Compute missing reference data mask
        NaNMask = ~(isnan(SRXSub) | isnan(SRYSub) | SRLabSub==0);
        
        SRXSub    = SRXSub   (NaNMask,:);
        SRYSub    = SRYSub   (NaNMask,:);
        SRLabSub  = SRLabSub (NaNMask,:);
        SRDispSub = SRDispSub(NaNMask,:);
        SRColSub  = SRColSub (NaNMask,:);

        % Interpolate label
        LabInt = griddata(SRXSub,SRYSub,SRLabSub,LFXq(xgv,ygv),LFYq(xgv,ygv),'nearest');
        
        %Interpolate disparity
        DispInt = griddata(SRXSub,SRYSub,SRDispSub,LFXq(xgv,ygv),LFYq(xgv,ygv),Method);
        
        % Combined depth + stencil buffer
        Mask = DispInt>LFDisp(xgv,ygv,1,v) & LabInt~=0;
        
        % Update light field label
        LabTemp = Label(xgv,ygv,1,v);
        LabTemp(Mask) = lab;
        Label(xgv,ygv,1,v) = LabTemp;
        
        % Update light field disparity
        DispTemp = LFDisp(xgv,ygv,1,v);
        DispTemp(Mask) = DispInt(Mask);
        LFDisp(xgv,ygv,1,v) = DispTemp;
        
        for c = 1:numChan
            %Interpolate color
            LFColInt = griddata(SRXSub,SRYSub,SRColSub(:,c),LFXq(xgv,ygv),LFYq(xgv,ygv),Method);
            
            % Update light field color
            LFColTemp = Color(xgv,ygv,c,v);
            LFColTemp(Mask) = LFColInt(Mask);
            Color(xgv,ygv,c,v) = LFColTemp;
        end
    end
end
fprintf('\n\n');

warning(wgriddata.state,'MATLAB:griddata:DuplicateDataPoints');
warning(wscattered.state,'MATLAB:scatteredInterpolant:DupPtsAvValuesWarnId');

LFRef = SR.FieldsToSet(Offset,Color,Label);

    function progress(msg,varargin)
        persistent sz
        if isempty(sz); sz = 0; end
        if nargin>1; sz = varargin{1}; end
        
        fprintf(repmat('\b',1,sz));
        fprintf(msg);
        sz = numel(msg)-count(msg,'\n');
    end
end