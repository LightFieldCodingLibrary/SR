function [PSNRout,backprojValue,backprojMask,backprojPos,backprojDispX,backprojDispY,...
    projValue,projMask,projPos,projDispX,projDispY]...
    = warpUnwarp(Value,Mask,Pos,DispX,DispY,varargin)
%WARPUNWARP Disparity Low-Rank update
%   Compute disparity update that best matches low-rank approximation

%% Parse input
p = inputParser; p.KeepUnmatched = true; p.StructExpand = true;
p.addParameter('keepCompleted', false     , @islogical);

p.parse(varargin{:});
keepCompleted = p.Results.keepCompleted;

%% Forward transform of original values
[projValue,projMask,projPos,projDispX,projDispY] = SR.warp(...
    Value,Mask,Pos,DispX,DispY,varargin{:},'direction','forward');

%%
if keepCompleted
    projMask = repmat(any(any(projMask,1),2),size(projMask,1),size(projMask,2),1,1);
end

%% Backward transform
[backprojValue,backprojMask,backprojPos,backprojDispX,backprojDispY] = SR.warp(...
    projValue,projMask,projPos,projDispX,projDispY,varargin{:},'direction','backward');

%% Remove unknown entries
projValue(~projMask) = nan;
projDispX(~projMask) = nan;
projDispY(~projMask) = nan;

%% Align original and projected values
[Value_,~,backProjValue_,backprojPos_] = align(Value,Pos,backprojValue,backprojPos);
backprojMask_ = align(backprojMask,backprojPos,backProjValue_,backprojPos_);
ValueMask_ = ~isnan(Value_);

%% Mask corresponding to original and reprojected values
Mask_ = ValueMask_&backprojMask_;

%% Compute outer PSNR
PSNRout = psnr(backProjValue_(Mask_),Value_(Mask_));
end

function [Val,ValPos,Mask,MaskPos] = align(Val,ValPos,Mask,MaskPos)
ValSize = size(Val);
MaskSize = size(Mask);

ValEnd = ValPos +ValSize;
MaskEnd = MaskPos+MaskSize;

minPos = min(ValPos,MaskPos);
maxPos = max(ValEnd,MaskEnd);

if islogical(Val)
    Val = padarray(Val,ValPos-minPos,false,'pre');
    Val = padarray(Val,maxPos-ValEnd,false,'post');
else
    Val = padarray(Val,ValPos-minPos,nan,'pre');
    Val = padarray(Val,maxPos-ValEnd,nan,'post');
end

if islogical(Mask)
    Mask = padarray(Mask,MaskPos-minPos,false,'pre');
    Mask = padarray(Mask,maxPos-MaskEnd,false,'post');
else
    Mask = padarray(Mask,MaskPos-minPos,nan,'pre');
    Mask = padarray(Mask,maxPos-MaskEnd,nan,'post');
end

[ValPos,MaskPos] = deal(minPos);
end