function [dX,dY,PSNRin,PSNRout,M,MMask,MPos,MDispX,MDispY,...
    projM,projValue,projMask,projPos,projDispX,projDispY] = ...
    warpLRAUpdate(Value,Mask,Pos,DispX,DispY,k,varargin)
%WARPLRAUPDATE warping + low rank approximation + update
%   Compute update that best matches low-rank approximation

[PSNRin,PSNRout,M,MMask,MPos,MDispX,MDispY,...
    projM,projValue,projMask,projPos,projDispX,projDispY]...
    = SR.warpLRA(Value,Mask,Pos,DispX,DispY,k,varargin{:});

[dX,dY] = SR.pinvUpdate(projValue,projMask,projM,varargin{:});

fprintf('PSNRin: %2.5f, PSNRout: %2.5f\n',PSNRin,PSNRout);
end