function Ref = multiply(Ref,C,method,varargin)
%MULTIPLY Summary of this function goes here
%   Detailed explanation goes here

[Offset,Color,Label] = SR.SetToFields(Ref);
Size = cellfun(@size,Color,'UniformOutput',false);
[ImgSize,ImgRes] = deal(cell(size(Color)));
RefSize = size(Ref);
numLab = numel(Label);

switch method
    case 'global'
        C_ = C;
        C = cell(1,numLab);
        C(:) = deal({C_});
end

if nargin>3
    ImgRes_ = varargin{1};
else
    ImgRes_= ImgRes;
    for lab = 1:numLab
        ImgRes_{lab} = [size(C{lab},2),1];
    end
end

for lab = 1:numLab
    Size{lab}(end+1:5) = 1;
    ImgSize{lab} = Size{lab}(1:3);
    ImgRes {lab} = Size{lab}(4:end);
    
    Color{lab} = reshape(Color{lab},prod(ImgSize{lab}),prod(ImgRes{lab}));
    Label{lab} = reshape(Label{lab},prod(ImgSize{lab}),prod(ImgRes{lab}));
end

switch method
    case 'global'
        Value = {Set2Matrix(Color)};
    case 'local'
        Value = Color;
end

for it = 1:numel(Value)
    Value{it} = Value{it}*C{it};
end

switch method
    case 'global'
        Color = Matrix2Set(Value{1},ImgSize,ImgRes_);
    case 'local'
        Color = Value;
end

for lab = 1:numLab
    Color{lab} = reshape(Color{lab},[ImgSize{lab},ImgRes_{lab}]);
    Label{lab} = lab.*double(~isnan(Color{lab}));
end

Color = reshape(Color,RefSize);

Ref = SR.FieldsToSet(Offset,Color,Label);

%% Auxiliary functions
    function Matrix = Set2Matrix(Set)
        Matrix = cell2mat(reshape(Set,[],1));
    end
       
    function Set = Matrix2Set(Matrix,ImgSize,ImgRes)
        for iter = 1:numel(ImgSize)
            ImgSize{iter} = prod(ImgSize{iter});
        end
        
        ImgSize = cell2mat(ImgSize);
        ImgRes = prod(ImgRes{1});
        
        Set = mat2cell(Matrix,ImgSize,ImgRes);
    end
end