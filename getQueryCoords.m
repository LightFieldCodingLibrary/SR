function [SRXq,SRYq,SRDisp] = getQueryCoords(LFRef,LFDisp,Method)
%LFTOSR Builds a super-ray set from a light-field
%   LFTOSR(LFSet)

%% Initialize constants and interpolants
% Lightfield properties
LFSize = size(LFRef.Color);
ImgSize = LFSize(1:3);
numChan = LFSize(3);
ImgRes = LFSize(4:end);
Offset = LFRef.Offset;
Label  = LFRef.Label;
centerView = floor(ImgRes/2)+1;
numView = prod(ImgRes);
numLab = max(Label(:));

% Initialize super-ray fields
[SROff,SRCol,SRLab,SRDisp,SRXq,SRYq] = deal(cell(numLab,1));

% Pad values
Label = padarray(Label,[1,1,0,0,0],nan,'both');
LFDisp = padarray(LFDisp,[1,1,0,0,0],nan,'both');
ImgSize(1:2) = ImgSize(1:2)+2;
Offset(1:2) = Offset(1:2)-1;
LFSize = [ImgSize,ImgRes];

seh = strel('arbitrary',[1,0,1]);
sev = strel('arbitrary',[1,0,1]');

Label = utils.fillmissing(Label,seh,1);
LFDisp = utils.fillmissing(LFDisp,seh,1);

Label = utils.fillmissing(Label,sev,1);
LFDisp = utils.fillmissing(LFDisp,sev,1);

% Replace missing values by zero (to avoid interpolation errors)
Label(isnan(Label))=0;
LFDisp(isnan(LFDisp))=0;

% Compute projected coordinates of reference samples
gv = arrayfun(@(x) 1:x,LFSize,'UniformOutput',false);
ugv = reshape(gv{4},1,1,1,[],1);
vgv = reshape(gv{5},1,1,1,1,[]);
ugv = ugv-centerView(1);
vgv = vgv-centerView(2);

[LFX,LFY] = ndgrid(gv{:});
LFX = LFX-ugv.*reshape(LFDisp,LFSize) + Offset(1);
LFY = LFY-vgv.*reshape(LFDisp,LFSize) + Offset(2);

progress('');

wgriddata = warning('query','MATLAB:griddata:DuplicateDataPoints');
wscattered = warning('query','MATLAB:scatteredInterpolant:DupPtsAvValuesWarnId');
warning('off','MATLAB:griddata:DuplicateDataPoints');
warning('off','MATLAB:scatteredInterpolant:DupPtsAvValuesWarnId');

%% Initialize super-rays
fprintf('Super-ray computation...\n');
for lab = 1:numLab
    msg = ['Initializing super-ray ',num2str(lab),'/',num2str(numLab),'\n'];
    progress(msg);
    
    % Compute super-ray grid boundary
    SRX = LFX(Label==lab);
    SRY = LFY(Label==lab);
    
    srxqmin = floor(min(SRX(:)));
    srxqmax = ceil (max(SRX(:)));
    sryqmin = floor(min(SRY(:)));
    sryqmax = ceil (max(SRY(:)));
    
    srxqgv = srxqmin:srxqmax;
    sryqgv = sryqmin:sryqmax;
    
    SROff{lab} = [srxqmin,sryqmin,1,1,1]-1;
    
    % Initialize super-ray color, label and disparities
    SRCol {lab} = nan([numel(srxqgv),numel(sryqgv),numChan,ImgRes]);
    SRLab {lab} = nan([numel(srxqgv),numel(sryqgv),1      ,ImgRes]);
    SRDisp{lab} = nan([numel(srxqgv),numel(sryqgv),1      ,ImgRes]);
    
    % Compute query grid for current super-ray
    [SRXq{lab},SRYq{lab}] = ndgrid(srxqgv,sryqgv);
    
    % Compute super-ray view by view
    for v = 1:numView
        msg = ['Interpolating super-ray ',num2str(lab),'/',num2str(numLab),...
            '\nView ',num2str(v),'/',num2str(numView),'\n'];
        progress(msg);
        
        % Reduce number of reference points for speed
        Mask = ...true(size(LFX(:,:,v)));
            LFX(:,:,v)>=min(srxqgv)-1 & LFX(:,:,v)<=max(srxqgv)+1 & ...
            LFY(:,:,v)>=min(sryqgv)-1 & LFY(:,:,v)<=max(sryqgv)+1;
        
        [~,mgv] = utils.tighten(Mask);
        
        LFXsub = LFX(mgv{:},v);
        LFYsub = LFY(mgv{:},v);
        
        [LFXsub,LFYsub,Xq,Yq] = utils.gridCoords(LFXsub,LFYsub,SRXq{lab},SRYq{lab});
        
        M = isnan(Xq)|isnan(Yq);
        
        Xq(M) = 1;
        Yq(M) = 1;
        
        % Interpolate disparity
        LFDispsub = LFDisp(mgv{:},v);
        temp = interp2(LFYsub,LFXsub,LFDispsub,Yq,Xq,Method);
        temp(M) = nan;
        SRDisp{lab}(:,:,1,v) = temp;
    end
    
    SRXq{lab} = SRXq{lab}+ugv.*SRDisp{lab};
    SRYq{lab} = SRYq{lab}+vgv.*SRDisp{lab};
end
fprintf('\n\n');

warning(wgriddata.state,'MATLAB:griddata:DuplicateDataPoints');
warning(wscattered.state,'MATLAB:scatteredInterpolant:DupPtsAvValuesWarnId');

% Create super-ray structure from fields

    function progress(msg,varargin)
        persistent sz
        if isempty(sz); sz = 0; end
        if nargin>1; sz = varargin{1}; end
        
        fprintf(repmat('\b',1,sz));
        fprintf(msg);
        sz = numel(msg)-count(msg,'\n');
    end
end