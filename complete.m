function [Ref,Value,Mask] = complete(Ref,method,varargin)
%COMPLETESET Summary of this function goes here
%   Detailed explanation goes here

[Offset,Color] = SR.SetToFields(Ref);
Size = cellfun(@size,Color,'UniformOutput',false);
[ImgSize,ImgRes] = cellfun(@(x) deal(x(1:3),x(4:end)),Size,'UniformOutput',false);
RefSize = size(Ref);
numLab = numel(Color);

for lab = 1:numLab
    Color{lab} = reshape(Color{lab},prod(ImgSize{lab}),[]);
end

switch method
    case 'global'
        Value = {Set2Matrix(Color)};
    case 'local'
        Value = Color;
end

Mask = Value;

for it = 1:numel(Value)
    [Value{it},Mask{it}] = utils.complete(Value{it},varargin{:});
end

switch method
    case 'global'
        Color = Matrix2Set(Value{1},ImgSize,ImgRes);
        Label = Matrix2Set(Mask {1},ImgSize,ImgRes);
    case 'local'
        Color = Value;
        Label = Mask;
end

for lab = 1:numLab
    Label{lab} = reshape(Label{lab},[ImgSize{lab},ImgRes{lab}]);
    Color{lab} = reshape(Color{lab},[ImgSize{lab},ImgRes{lab}]);
    
    Label{lab} = lab*double(any(Label{lab},3));
end

Color = reshape(Color,RefSize);
Label = reshape(Label,RefSize);

Ref = SR.FieldsToSet(Offset,Color,Label);

%% Auxiliary functions
    function Matrix = Set2Matrix(Set)
        Matrix = cell2mat(reshape(Set,[],1));
    end

    function Set = Matrix2Set(Matrix,ImgSize,ImgRes)
        for iter = 1:numel(ImgSize)
            ImgSize{iter} = prod(ImgSize{iter});
        end
        
        ImgSize = cell2mat(ImgSize);
        ImgRes = prod(ImgRes{1});
        
        Set = mat2cell(Matrix,ImgSize,ImgRes);
    end
end
