function [SRXq,SRYq,SRDisp,dispParams] = getQueryCoordsAffine(LFRef,LFDisp)
%GETQUERYCOORDSAFFINE Summary of this function goes here
%   Detailed explanation goes here
%%

%% Initialize constants and interpolants
% Lightfield properties
LFSize = size(LFRef.Color);
ImgRes = LFSize(4:end);
Label  = LFRef.Label;
centerView = floor(ImgRes/2)+1;
numLab = max(Label(:));

% Replace missing values by zero (to avoid interpolation errors)
Label(isnan(Label))=0;
LFDisp(isnan(LFDisp))=0;

% Compute projected coordinates of reference samples
gv = arrayfun(@(x) 1:x,LFSize,'UniformOutput',false);
gv{3} = 1;
ugv = reshape(gv{4},1,1,1,[],1);
vgv = reshape(gv{5},1,1,1,1,[]);
ugv = ugv-centerView(1);
vgv = vgv-centerView(2);

%%
[X,Y,~,U,V] = ndgrid(gv{:});
U = U-centerView(1);
V = V-centerView(2);
Xp = X-U.*LFDisp;
Yp = Y-V.*LFDisp;

%%
s = cell(1,numLab);
[SRXq,SRYq,SRDisp] = deal(cell(1,numLab));

%%
fprintf('Super-ray sampling coordinates computation...\n');

%%
progress('',0);
for lab = 1:numLab
    msg = ['Initializing super-ray ',num2str(lab),'/',num2str(numLab),'\n'];
    progress(msg);
    
    M = Label==lab;
    X_ = Xp(M(:));
    Y_ = Yp(M(:));
    D_ = LFDisp(M(:));
    
    s{lab} = fit([X_,Y_],D_,'poly11','Robust','Bisquare');
    a = s{lab}.p10;
    b = s{lab}.p01;
    c = s{lab}.p00;
    
    X_ = X(M(:));
    Y_ = Y(M(:));
    U_ = U(M(:));
    V_ = V(M(:));
    
    tX = X_-U_.*(a.*X_+b.*Y_+c)./( 1+a.*U_+b.*V_);
    tY = Y_-V_.*(a.*X_+b.*Y_+c)./( 1+a.*U_+b.*V_);
    
    srxgv = floor(min(tX(:))):ceil(max(tX(:)));
    srygv = floor(min(tY(:))):ceil(max(tY(:)));
    
    [X_,Y_] = ndgrid(srxgv,srygv);
    D_ = s{lab}([X_(:),Y_(:)]);
    D_ = reshape(D_,size(X_));
    
    [SRXq{lab},SRYq{lab}] = ndgrid(srxgv,srygv,gv{3:end});
    SRXq{lab} = SRXq{lab}+ugv.*D_;
    SRYq{lab} = SRYq{lab}+vgv.*D_;
    
    SRDisp{lab} = ones(size(SRXq{lab})).*D_;
end

progress('',0);
fprintf('\n');

p00 = cellfun(@(x) x.p00,s);
p01 = cellfun(@(x) x.p01,s);
p10 = cellfun(@(x) x.p10,s);

dispParams = [p00;p10;p01];

    function progress(msg,varargin)
        persistent sz
        if isempty(sz); sz = 0; end
        if nargin>1; sz = varargin{1}; end
        
        fprintf(repmat('\b',1,sz));
        fprintf(msg);
        sz = numel(msg)-count(msg,'\n');
    end
end