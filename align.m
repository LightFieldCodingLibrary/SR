function [Ref,Rec] = align(Ref,Rec)
%ALIGN Summary of this function goes here
%   Detailed explanation goes here

[OffRef,ColRef,LabRef] = SR.SetToFields(Ref);
[OffRec,ColRec,LabRec] = SR.SetToFields(Rec);

numLab = numel(LabRef);

for lab = 1:numLab
    LabRef{lab}(LabRef{lab}==0) = nan;
    LabRec{lab}(LabRec{lab}==0) = nan;
    
    [Cols,Offs,Labs] = utils.align({ColRef{lab},ColRec{lab}},...
        {OffRef{lab},OffRec{lab}},{LabRef{lab},LabRec{lab}});
    
    [ColRef{lab},ColRec{lab}] = deal(Cols{:});
    [OffRef{lab},OffRec{lab}] = deal(Offs{:});
    [LabRef{lab},LabRec{lab}] = deal(Labs{:});    
end

Ref = SR.FieldsToSet(OffRef,ColRef,LabRef);
Rec = SR.FieldsToSet(OffRec,ColRec,LabRec);

end